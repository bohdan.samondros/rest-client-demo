package example.domain;

import java.util.Objects;

public class PhoneCard {

    private long id;

    private String phone;

    private String name;

    public PhoneCard() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneCard phoneCard = (PhoneCard) o;
        return id == phoneCard.id &&
                Objects.equals(phone, phoneCard.phone) &&
                Objects.equals(name, phoneCard.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone, name);
    }

    @Override
    public String toString() {
        return "PhoneCard{" +
                "id=" + id +
                ", number='" + phone + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
