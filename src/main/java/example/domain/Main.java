package example.domain;

import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import java.util.List;

public class Main {

    private static Client client = ClientBuilder.newClient();

    public static final String RESOURCE_URL = "http://localhost:8080/phone-card/api/phone-cards";

    public static void main(String[] args) {
        demoPost();

        demoGetAll();
    }

    private static void demoGetAll() {
        WebTarget target = client.target(RESOURCE_URL);

        List<PhoneCard> response = target.request()
                .accept(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<PhoneCard>>(){});

        System.out.println(response);
    }

    private static void demoPost() {
        WebTarget target = client.target(RESOURCE_URL);

        PhoneCard phoneCard = new PhoneCard();
        phoneCard.setName("REST Client Card");
        phoneCard.setPhone("5555");

        Response response = target.request()
                .post(Entity.entity(phoneCard, MediaType.APPLICATION_JSON));

        System.out.println(response);
    }
}
